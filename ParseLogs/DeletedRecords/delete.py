import os, sys
from time import strftime

today = strftime("%Y-%m-%d")
path = sys.argv[1]
extract = file("DELETE_"+today+".csv", "a")
extract.write('fileName,action,parentName,parentId,refCell,childName,childId\n')
for root, dirs, files in os.walk(path):
    for _file in files:
        if not os.path.isdir(_file):
            if(_file.find(today) > -1):
                fileName=root+'/'+_file
                print fileName
                log = file(fileName)
                for line in log:
                    if "DELETE {'action'" in line:
                        deldict = eval(line[line.index('{'):line.index('}')+1])
                        extract.write('%s,%s,%s,%s,%s,%s,%s\n' %(_file,deldict['action'],deldict['parentName'],deldict['parentId'],deldict['refCell'],deldict['childName'],deldict['childId']))
                log.close()
extract.close()
print 'Processing complete.'
@ECHO off
REM SERVER IP CONFIG
SET app1=0.0.0.0
SET app2=0.0.0.0
SET app3=0.0.0.0

REM LOGIN USER CONFIG
SET app1user=uid
SET app2user=uid
SET app3user=uid

REM LOGIN PASSWORD CONFIG
SET app1pass=pass
SET app2pass=pass
SET app3pass=pass

SET day = %date:~10,4%%date:~4,2%%date:~7,2%

@ECHO on

pscp -pw %app1pass% ./UniqueLogins.py %app1user%@%app1%:/home/%app1user%/
pscp -pw %app2pass% ./UniqueLogins.py %app2user%@%app2%:/home/%app2user%/
pscp -pw %app3pass% ./UniqueLogins.py %app3user%@%app3%:/home/%app3user%/

plink -ssh -pw %app1pass% -noagent -m cmd1.cmd %app1user%@%app1%
plink -ssh -pw %app2pass% -noagent -m cmd1.cmd %app2user%@%app2%
plink -ssh -pw %app3pass% -noagent -m cmd1.cmd %app3user%@%app3%

pscp -pw %app1pass% %app1user%@%app1%:/home/%app1user%/UniqueLogins_*.csv ./dc-erp-app1
pscp -pw %app2pass% %app2user%@%app2%:/home/%app2user%/UniqueLogins_*.csv ./dc-erp-app2
pscp -pw %app3pass% %app3user%@%app3%:/home/%app3user%/UniqueLogins_*.csv ./dc-erp-app3

plink -ssh -pw %app1pass% -noagent -m cmd2.cmd %app1user%@%app1%
plink -ssh -pw %app2pass% -noagent -m cmd2.cmd %app2user%@%app2%
plink -ssh -pw %app3pass% -noagent -m cmd2.cmd %app3user%@%app3%

UniqueLogins.py consolidate

pause
import os, sys, socket
from threading import Thread
from time import strftime

host    = socket.gethostname().replace(".","").replace("-","")
apps    =["app1","app2","app3","app4","app5","app6"] #logfile patterns for different apps
pattern = strftime("%Y-%m-%d")
path    = ''

if sys.argv[1]=='collect':
    try:
        path = sys.argv[2]
    except:
        print 'Please provide the path to the log files'
        sys.exit

def getUniqueLogins(path, pattern, app):
    counter = 0
    for root, dirs, files in os.walk(path):
        for _file in files:
            if not os.path.isdir(_file):
                if(_file.find(app) > -1):
                    if(_file.find(pattern) > -1):
                        fileName=root+'/'+_file
                        log = file(fileName)
                        for line in log:
                            if "Creating New instance for the model UserLoginModel with PrimaryKey" in line:
                                counter = counter + 1
                        log.close()
    outputWriter(app + '=' + str(counter) + '\n')

def outputWriter(content):
    extract = file('Unique_Logins_' + host + ".py", "a")
    extract.write(content + '\n')
    extract.close()
    
def init(action):
    if action == 'collect':
        for app in apps:
            try:
                t= Thread(target=getUniqueLogins, args=(path, pattern, app), name=app)
                t.start()
            except:
                print 'Thread for %s failed to complete' % (t.name) 
                raise
            
    if action == 'consolidate':
        #modify the hostnames as required
        import Unique_Logins_host1 as app1
        import Unique_Logins_host2 as app2
        import Unique_Logins_host3 as app3

        report = file(pattern,'w')
        report.write('app1 : %d\n' % (app1.app1 + app2.app1 + app3.app1))
        report.write('app2 : %d\n' % (app1.app2 + app2.app2 + app3.app2))
        report.write('app3 : %d\n' % (app1.app3 + app2.app3 + app3.app3))
        report.write('app4 : %d\n' % (app1.app4 + app2.app4 + app3.app4))
        report.write('app5 : %d\n' % (app1.app5 + app2.app5 + app3.app5))
        report.write('app6 : %d\n' % (app1.app6 + app2.app6 + app3.app6))
        report.close()
    
    print 'processing complete.'

init(sys.argv[1])    

'''
    acript to automatically download log files of previous hour from Production
    putty, plink & pscp should be included in the PATH
    Cisco VPN install directory also to be added to PATH.
    vpnclient ref : http://www.cisco.com/en/US/docs/security/vpn_client/cisco_vpn_client/vpn_client46/administration/guide/vcAch5.html
'''
VPNPROFILE  = '' #cisco vpn profile name
VPNUSER     = '' #VPN user
VPNPASSWORD = '' #VPN password

HOME = '' #home directory in the server

#Server IP Address
APP1 = '0.0.0.0'
APP2 = '0.0.0.0'
APP3 = '0.0.0.0'

#Server login usernames
APP1USER = ''
APP2USER = ''
APP3USER = ''

#server login passwords
APP1PASS = ''
APP2PASS = ''
APP3PASS = ''

from datetime import datetime
now = datetime.now()
print now

def two_digit_value(val):
    if len(str(val)) < 2:
        return '0'+str(val)
    else:
        return str(val)

CYEAR  = str(now.year)
CMONTH = two_digit_value(now.month)
CDAY   = two_digit_value(now.day)
CHOUR  = two_digit_value(now.hour-1)

print '%s, %s, %s, %s' % (CYEAR, CMONTH, CDAY, CHOUR)

FINANCE = 'finance.log.%s-%s-%s-%s.*' % (CYEAR, CMONTH, CDAY, CHOUR)
HRMS    = 'hrms.log.%s-%s-%s-%s.*' % (CYEAR, CMONTH, CDAY, CHOUR)

from subprocess import call
#cmd = 'vpnclient connect %s user %s pwd %s >vpnconnect.log' % (VPNPROFILE, VPNUSER, VPNPASSWORD)
''' Problem with redirection of logs to a file with vpnclient direct command,  so constructed a batch to do it '''
cmd = 'vpnconnect.bat %s %s %s' % (VPNPROFILE, VPNUSER, VPNPASSWORD)
print cmd
retcode = call(cmd)
print 'VPNCLIENT CONNECTION EXIT CODE : %s' % (retcode)
'''
RETURN CODES
14  = Connection already exits / Connection Failed
200 = Connection established Successfully
'''
connected=''
if retcode == 200:
    connected = True
elif retcode ==14:
    connectlog = open('connect.log')
    for line in connectlog:
        if 'Reason: Failed to establish a VPN connection' in line:
            connected = False
        elif 'Reason: A connection already exists' in line:
            connected = True
    connectlog.close()

if connected:
    ''' VPN CONNECTION ESTABLISHED SUCCESSFULLY '''
    ''' Compressing the log files '''
    i=[1,2,3]
    for x in i:
        fileName = 'app' + str(x) + '.1.cmd'
        appcmd = open(fileName,'w')
        cmd1 = 'tar cvzf finance-app%s-%s-%s-%s-%s.tgz /logs/appsrv/dc-erp-app%s-instance1/%s' % (str(x), CYEAR, CMONTH, CDAY, CHOUR, str(x), FINANCE)
        cmd2 = 'tar cvzf hrms-app%s-%s-%s-%s-%s.tgz /logs/appsrv/dc-erp-app%s-instance1/%s' % (str(x), CYEAR, CMONTH, CDAY, CHOUR, str(x), HRMS)
        print cmd1, cmd2
        appcmd.write(cmd1+'\n')
        appcmd.write(cmd2+'\n')
        appcmd.close()
            
    APP1CMD = 'plink.exe -ssh -pw %s -noagent -m app1.1.cmd %s@%s' % (APP1USER, APP1PASS, APP1)
    APP2CMD = 'plink.exe -ssh -pw %s -noagent -m app2.1.cmd %s@%s' % (APP2USER, APP2PASS, APP2)
    APP3CMD = 'plink.exe -ssh -pw %s -noagent -m app3.1.cmd %s@%s' % (APP3USER, APP3PASS, APP3)
    
    print APP1CMD
    app1retcode = call(APP1CMD)
    print 'APP1 Compression EXIT Code : ',app1retcode
    
    print APP2CMD 
    app2retcode = call(APP2CMD)
    print 'APP2 Compression EXIT Code : ',app2retcode
    
    print APP3CMD 
    app3retcode = call(APP3CMD)
    print 'APP3 Compression EXIT Code : ',app3retcode
    
    ''' Compression of the previous hour logs complete '''
    ''' Downloading the compressed filed '''
    down1cmd1 = 'pscp -pw %s anandr@%s:/home/%s/finance-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP1PASS, APP1, APP1USER, '1', CYEAR, CMONTH, CDAY, CHOUR)
    down1cmd2 = 'pscp -pw %s anandr@%s:/home/%s/hrms-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP1PASS, APP1, APP1USER, '1', CYEAR, CMONTH, CDAY, CHOUR)
    down2cmd1 = 'pscp -pw %s anandr@%s:/home/%s/finance-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP2PASS, APP2, APP2USER, '2', CYEAR, CMONTH, CDAY, CHOUR)
    down2cmd2 = 'pscp -pw %s anandr@%s:/home/%s/hrms-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP2PASS, APP2, APP2USER, '2', CYEAR, CMONTH, CDAY, CHOUR)
    down3cmd1 = 'pscp -pw %s anandr@%s:/home/%s/finance-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP3PASS, APP3, APP3USER, '3', CYEAR, CMONTH, CDAY, CHOUR)
    down3cmd2 = 'pscp -pw %s anandr@%s:/home/%s/hrms-app%s-%s-%s-%s-%s.tgz D:\\support\\logs' % (APP3PASS, APP3, APP3USER, '3', CYEAR, CMONTH, CDAY, CHOUR)
    
    print down1cmd1
    ret = call(down1cmd1)
    print 'FIN APP1 EXIT CODE : ',ret
    
    print down1cmd2
    ret = call(down1cmd2)
    print 'HRMS APP1 EXIT CODE : ',ret
    
    print down2cmd1
    ret = call(down2cmd1)
    print 'FIN APP2 EXIT CODE : ',ret
    
    print down2cmd2
    ret = call(down2cmd2)
    print 'HRMS APP2 EXIT CODE : ',ret
    
    print down3cmd1
    ret = call(down3cmd1)
    print 'FIN APP3 EXIT CODE : ',ret
    
    print down3cmd2
    ret = call(down3cmd2)
    print 'HRMS APP3 EXIT CODE : ',ret
    
    ''' Downloading of Compressed log files complete '''
    ''' Deleting the log file from the server '''
    i=[1,2,3]
    for x in i:
        fileName = 'app' + str(x) + '.2.cmd'
        appcmd = open(fileName,'w')
        cmd1 = 'rm /home/%s/finance-app%s-%s-%s-%s-%s.tgz' % (APP1USER, str(x), CYEAR, CMONTH, CDAY, CHOUR)
        cmd2 = 'rm /home/%s/hrms-app%s-%s-%s-%s-%s.tgz' % (APP1USER, str(x), CYEAR, CMONTH, CDAY, CHOUR)
        appcmd.write(cmd1+'\n')
        appcmd.write(cmd2+'\n')
        appcmd.close()

    APP1CMD = 'plink.exe -ssh -pw %s -noagent -m app1.2.cmd %s@%s' % (APP1USER, APP1PASS, APP1)
    APP2CMD = 'plink.exe -ssh -pw %s -noagent -m app2.2.cmd %s@%s' % (APP2USER, APP2PASS, APP2)
    APP3CMD = 'plink.exe -ssh -pw %s -noagent -m app3.2.cmd %s@%s' % (APP3USER, APP3PASS, APP3)
    
    print APP1CMD
    app1retcode = call(APP1CMD)
    print 'APP1 DELETE EXIT Code : ',app1retcode
    
    print APP2CMD 
    app2retcode = call(APP2CMD)
    print 'APP2 DELETE EXIT Code : ',app2retcode
    
    print APP3CMD 
    app3retcode = call(APP3CMD)
    print 'APP3 DELETE EXIT Code : ',app3retcode

    
    import os
    os.remove('connect.log')
    os.remove('app1.1.cmd')
    os.remove('app1.2.cmd')
    os.remove('app2.1.cmd')
    os.remove('app2.2.cmd')
    os.remove('app3.1.cmd')
    os.remove('app3.2.cmd')
    
''' THE DILEMA : TO DISCONNECT OR NOT TO DISCONNECT '''
if retcode != 14:
    call('vpnclient disconnect')

print 'set complete for %s %s %s %s' % (CYEAR, CMONTH, CDAY, CHOUR)
print datetime.now()
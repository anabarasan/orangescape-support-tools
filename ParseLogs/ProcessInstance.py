import os, sys

if (len(sys.argv) < 2):
    print "usage : python ProcessInstance.py appname recordId modelName logFilePath"
else:
    emId = sys.argv[2] #RECORD ID
    modelName = sys.argv[3] #ModelName
    path = sys.argv[4] #logfile location
    #path = 'D:/support/logs/logs/appsrv/dc-erp-app2-instance1'
    #path = 'D:/support/logs/logs/appsrv/dc-erp-app3-instance1'
    #path = 'D:/logs/appsrv/server'
    extract = file("PI_"+sys.argv[1]+".csv", "w")
    extract.write('fileName,PI\n')
    for root, dirs, files in os.walk(path):
        for _file in files:
            if not os.path.isdir(_file):
                if(_file.find(sys.argv[1]) > -1):
                    fileName=root+'/'+_file
                    log = file(fileName)
                    for line in log:
                        if ('PI created. Model:'+modelName+', InstanceId:'+emId in line) or ('PI updated. Model:'+modelName+', InstanceId:'+emId in line) or ('PI before commit. Model:'+modelName+', InstanceId:'+emId) in line or ('PI after commit. Model:'+modelName+', InstanceId:'+emId in line):
                        #if ('PI created' in line) or ('PI updated' in line) or ('PI before' in line) or ('PI after' in line):
                        #if 'Assigning activity Waiting For Verification' in line:
                            extract.write('"%s","%s"\n'%(fileName,line))
                    log.close()
    extract.close()
    print 'Processing complete.'
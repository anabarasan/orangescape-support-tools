import os, re, sys

def getErrors(app, path):
    pattern = re.compile('\d\d\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s\d{4}\s\d\d:\d\d:\d\d,\d{3}\s\S*\sERROR\s\S*\s')
    #path = 'D:/support/logs/logs/appsrv'
    extract = file("ERROR_"+app+".csv", "w")
    extract.write('fileName,Error\n')
    for root, dirs, files in os.walk(path):
        for _file in files:
            if not os.path.isdir(_file):
                if(_file.find(app) > -1):
                    fileName=root+'/'+_file
                    log = file(fileName)
                    for line in log:
                        if pattern.findall(line):
                            if 'The input xml is' not in line and 'the request xml is' not in line and 'Webservice return code' not in line:
                                if 'the response xml is' not in line and 'the response dict is' not in line:
                                    if 'Arg 2. Invalid sort order' not in line and 'Arg 5. Invalid sort order' not in line:
                                        print fileName
                                        #extract.write('"%s","%s"'%(fileName,line))
                                        extract.write(line)
                    log.close()
    extract.close()
    print 'Processing complete.'
    
#getErrors(sys.argv[1], sys.argv[2])
getErrors('finance', 'D:\\support\\logs\\20120612\\logs\\appsrv')
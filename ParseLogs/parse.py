import os, sys

def ProcessInstance(path, pattern, Model=None, Id=None):
    
    if path is None or len(path) == 0 or pattern is None or len(pattern) == 0 :
        print 'usage : ProcessInstance(log_path, file_pattern, model, id)'
        sys.exit()
    
    searchCreate = 'PI created.'
    searchModify = 'PI updated.'
    searchBefore = 'PI before update.'
    searchAfter  = 'PI after update.'
    
    if Model is not None:
        searchCreate = searchCreate + ' Model:' + Model
        searchModify = searchModify + ' Model:' + Model
        searchBefore = searchBefore + ' Model:' + Model
        searchAfter  = searchAfter  + ' Model:' + Model
    
    if Model is not None and Id is not None:
        searchCreate = searchCreate + ' InstanceId:' + Model
        searchModify = searchModify + ' InstanceId:' + Model
        searchBefore = searchBefore + ' InstanceId:' + Model
        searchAfter  = searchAfter  + ' InstanceId:' + Model
        
    try:
        extract = file("PI_" + pattern + ".csv", "w")
        extract.write('fileName,PI\n')
        for root, dirs, files in os.walk(path):
            for _file in files:
                if not os.path.isdir(_file):
                    if(_file.find(pattern) > -1):
                        fileName=root+'/'+_file
                        print fileName
                        log = file(fileName)
                        for line in log:
                            if (searchCreate in line) or (searchModify in line) or (searchBefore in line) or (searchAfter in line):
                                extract.write('"%s","%s"\n'%(fileName,line))
                        log.close()
        extract.close()
        print 'Processing complete.'
    except:
        print 'Error....'
        raise

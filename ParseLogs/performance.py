from datetime import datetime
now = datetime.now()

def two_digit_value(val):
    if len(str(val)) < 2:
        return '0'+str(val)
    else:
        return str(val)

CYEAR  = str(now.year)
CMONTH = two_digit_value(now.month)
CDAY   = two_digit_value(now.day)

import os
def getPerformanceDetails(asset,path,file_):
    extract = file(file_, "w")
    if asset=='MEM':
        extract.write('                                                total       used       free\n')
    for root, dirs, files in os.walk(path):
        for _file in files:
            if not os.path.isdir(_file):
                if(_file.find(file_) > -1):
                    fileName=root+'/'+_file
                    log = file(fileName)
                    for line in log:
                        if (asset=='CPU' and "Cpu(s):" in line) or (asset=='MEM' and "Total:" in line):
                                extract.write('%s | %s' % (fileName,line))
                    log.close()
    extract.close()
    print 'processing for %s complete.' % (asset)
    
CPU  = 'cpu.%s%s%s'%(CYEAR, CMONTH, CDAY)
MEM  = 'mem.%s%s%s'%(CYEAR, CMONTH, CDAY)    
PATH = '.'

getPerformanceDetails('CPU', PATH, CPU)
getPerformanceDetails('MEM', PATH, MEM)
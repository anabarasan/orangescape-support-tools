import os, sys

def getSheetMetadata(SheetId):
    exec 'from appzip.orangescape.application.%s import %s as Model'% (SheetId, SheetId)
    return Model

def default():
    return ''

def getSheetName(SheetId):
    try:
        return getSheetMetadata(SheetId)[SheetId]["SheetName"]
    except:
        default()
    
try:
    path = sys.argv[1]
except:
    path = ".\\appzip\\orangescape\\static\\jstl"
    
outfile = file('forceSubmit.csv','w')
outfile.write('SheetMetadataId, fileName, CellName, forceSubmit\n')
for root, dirs, files in os.walk(path):
    for _file in files:
        if not os.path.isdir(_file):
            fileName = root + '/' + _file
            jstl = file(fileName)
            for line in jstl:
                if 'renderWidget(' in line:
                    startPos = line.find('renderWidget')+12
                    endPos = line.rfind(')')+1
                    WidgetProperties = line[startPos:endPos]
                    properties = WidgetProperties.split(',')
                    
                    sheetMetadataId = root.split('\\')[len(root.split('\\'))-1]
                    cellName = properties[2].replace("'",'')
                    nodeCount = int(properties[7].replace("'",''))
                    try:
                        forceSubmit = int(properties[len(properties)-1].replace(')','').strip())
                    except:
                        forceSubmit = 0
                        
                    if (forceSubmit > 0 or nodeCount > 0):
                        result = True
                    else:
                        result = False
                    
                    outfile.write('%s, %s, %s, %s\n' % (getSheetName(sheetMetadataId), _file, cellName, result))
outfile.close

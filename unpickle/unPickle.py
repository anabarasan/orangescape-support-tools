import sys
import os
import cPickle as pickle
import logging

def unPickle(filepath):

    for root, dirs, files in os.walk(os.path.join(filepath,"out")):
        for f in files:
            os.unlink(os.path.join(root, f))

    pck = os.path.join(filepath,"in")
    for root, dirs, files in os.walk(pck):
        for f in files:
            try:
                filename, ext = f.split(".")
                print f
                pickled_file_path=os.path.join(filepath,"in",f)
                pickled_file = open(pickled_file_path,"rb")
                org_file = pickle.load(pickled_file)
                app = file(os.path.join(filepath,'out',filename+'.py'), "w")
                app.write(filename+'='+str(org_file))
                app.close()
            except Exception, e:
                logging.error("Error processing file %s : %s" % (f,e))

unPickle(sys.argv[1])
import os, sys

def formatline(linestring):

    no_of_chars_to_replace = linestring.find("- [")+2
    i=0
    replacechar=""
    while i < no_of_chars_to_replace:
        replacechar = replacechar + '\t'
        i = i+1
    return replacechar+linestring[no_of_chars_to_replace:len(linestring)], no_of_chars_to_replace

try:
    fileName = sys.argv[1]
except:
    fileName = 'perf-monitor-payroll.txt'

extract = file(fileName + '-formatted', "w")
log = file(fileName)
for line in log:
    #if ('Component[executeAction] Action:' in line) or ('ProcessEngine.execute().resolve' in line) or ('ProcessEngine.execute().start' in line):
    #if ('[0.000]' not in line) or ('ProcessEngine.execute().resolve' in line):
        extract.write('%s'%(formatline(line)[0]))
log.close()
extract.close()
print 'Processing complete.'
bol = ' INFO  WSGILogger : +++++ HTTP_USERID : '
eol = 'DbSession requet /SheetMetadata_a96e3a40_4982_4b88_91ec_87a8fbb983f3/Acb3c2a62d05c2470e94245b87d71f019b/SheetMetadata_a96e3a40_4982_4b88_91ec_87a8fbb983f3/MonthlyP_'
logPathPrefix = '/logs/appsrv/dc-erp-app4-instance1/'
#logPathPrefix = ''

'''
use the below formulas for the mentioned columns in excel
TstartTime      hh:mm:ss        =left(substitute(C2,left(C2,12),""),find(",",substitute(C2,left(C2,12),""))-1)
TendTime        hh:mm:ss        =left(substitute(D2,left(D2,12),""),find(",",substitute(D2,left(D2,12),""))-1)
startTimeNumber number(20 dec)  =time(hour(G2),minute(G2),second(G2))
endTimeNumber   number(20 dec)  =time(hour(H2),minute(H2),second(H2))
differentiator  number          =if(iserr(I2),-1,if(J2>I2,1,0))
timeDiff        hh:mm:ss        =if(K2=-1,-1,J2-I2)
Hours           number          =hour(L2)
Minutes         number          =minute(L2)
Seconds         number          =second(L2)
TotalSeconds    number          =M2*60*60+N2*60+O2
'''

def getThreadId(logline):
    return logline[25:logline.find(' ',25)]
    
def getTimeStamp(logline):
    return logline[0:24]
    
def getTransactionId(logline):
    return logline[logline.find('MonthlyP_'):logline.find('MonthlyP_')+45]
    
def getLogFileList():
    loglist = open('payrolllist')
    logfiles = []
    for line in loglist:
        logfiles.append(line)
    return logfiles

def getTransactions():
    loglist = getLogFileList()
    
    transactions = open('Transactions.csv','w')
    transactions.write('ThreadId, TransactionId, startTime, EndTime, startFile, endFile, TstartTime, TendTime, startTimeNumber, endTimeNumber, differentiator, timeDiff, Hours, Minutes, Seconds, TotalSeconds\n')
    
    x=0
    while x < len(loglist):
        fila = logPathPrefix+loglist[x].rstrip('\n')
        print fila
        logfile = open(logPathPrefix+loglist[x].rstrip('\n'))
        lineno = 0
        for line in reversed(logfile.readlines()):
            if eol in line:
                threadId = getThreadId(line)
                endTime = getTimeStamp(line)
                transactionId = getTransactionId(line)
                startTimeDetails = getStartTime(loglist, threadId, lineno, x)
                startTime = startTimeDetails[0]
                startTimeFileNo = startTimeDetails[1]
                if startTimeFileNo == '':
                    startTimeFile = 'unable to determine'
                else:
                    startTimeFile = loglist[startTimeFileNo].rstrip('\n')
                transactions.write('"%s", "%s", "%s", "%s", "%s", "%s"\n' % (threadId, transactionId, startTime, endTime, loglist[x].rstrip('\n'), startTimeFile))
            lineno = lineno + 1
        logfile.close()
        x = x + 1
        
    transactions.close()
                
def getStartTime(loglist, threadId, startLineNo, x):
    result = 'False'
    searchLine = threadId + bol
    startTime = ''
    
    #print '\t %s' % (loglist[x].rstrip('\n'))
    logfile = open(logPathPrefix+loglist[x].rstrip('\n'))
    lineno = 0
    for line in reversed(logfile.readlines()):
        if lineno > startLineNo and searchLine in line:
            startTime = getTimeStamp(line)
            result = 'True'
            break
        lineno = lineno + 1
    logfile.close()
    
    if result == 'False':
        #print 'recursion call'
        getStartTime(loglist, threadId, 0, x+1)

    return startTime,x
    
getTransactions()